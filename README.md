## Que fait la commande ?
La commande si dessous clone simplement ce dépot pour creer un nouveau projet.<br/>
Cela vas vous créer rappidement une base légère pour commencer à coder un site web.<br/>
En prime je vous met une commande "**gitf**" qui vous fera tout simplement les action ci-dessous en une fois.<br/>
- git add .
- git commit -m "*message*"
- git push origine <*branche actuelle*>

C'est à dire:

- création du dossier assets pour tout les fichiers qui seront inclus à notre HTML
- Générer un index.html configuré pour HTML5 et inclure les fichiers app.css, app.js et la librairie de fontawesome 
- Générer app.css avec la suppression des style appliqué par defaut, changement de la police et taille de police par defaut.
- Générer app.js avec une seule fonction vierge qui permet d'attendre que le DOM soit chargé pour être exécuter.
- Générer un favicon juste pour faire classe

## Comment l'ajouter ?

- La fonction "**np**" ci-dessous doit-être ajouté dans le fichier **~/.bash_aliase** ou **~/.bashrc** s'il n'existe pas.
- Modifier **user=*MON_USER*** et **passwd=*TOKEN***
- Redemarrer le terminal

## Les fonctions
```
np() {
	if [ -z  $1 ]
	then
		printf "\033[0;31mERROR:\033[0m argument missing please type (np ProjectName)\n"
	else
		if [ -d $1 ]
		then
			printf "\033[0;31mERROR:\033[0m \"$1\" already exist!\n"
		else
			
			user=MON_USER
			passwd=MON_TOKEN
		
			git clone https://gitlab.com/bourcierlaurent/Gen_Template $1
			cd $1
			
			> README.md
			git remote remove origin
			git remote add origin https://$user:$passwd@gitlab.com/$user/$1

			git add .
			git commit -m "Inital commit"
			gitBranch=$(git branch | awk '{print $2}')
			git push origin $gitBranch

			code . index.html assets/css/app.css assets/js/app.js &
		fi    
	fi
}

gitf() {
	gitBranch=$(git branch | awk '{print $2}')
	if [ -z  $gitBranch ]
	then
		clear
		printf "\033[0;31mErreur:\033[0m Vous n'êtes pas dans un projet\n"
	else
		while [ -z $comment ]
		do
			echo "Commit comment: "
			read -a comment
			clear
		done
	
		git add .
		git commit -m "${comment[*]}"
		git push origin $gitBranch
	fi
	comment=""
	gitBranch=""
}
```
## Comment l'utiliser ?

**Attention a ne pas mettre d'espace dans le nom du projet.**

**np** *nom_du_projet*